extends StoryNode
class_name DialogueLine

@export_multiline var text: String
@export var voice_acting: AudioStreamMP3

var finished: bool = true

func run():
	finished = false
	DialogueManager.play_dialogue(self)

func lineEnded():
	if(!finished):
		super.run()
		finished = true

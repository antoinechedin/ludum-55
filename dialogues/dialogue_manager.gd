extends Node

signal line_emited

#-----------------------------
## -
#-----------------------------
func play_dialogue(line: DialogueLine):
	line_emited.emit(line)

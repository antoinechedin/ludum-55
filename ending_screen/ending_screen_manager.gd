extends Node

@export var root_ui: Control
@export var black_background: ColorRect
@export var label: Label
@export var restart_button: Button
@export var endings_event: Array[GameEvent]
@export var endings_resource: Array[Ending]

#-----------------------------
## -
#----------------------------
func _ready() -> void:
	root_ui.hide()
	restart_button.pressed.connect(_on_restart_pressed)
	for i in endings_event.size():
		endings_event[i].subscribe(_on_ending_event_invoken.bind(i))

#-----------------------------
## -
#----------------------------
func _on_ending_event_invoken(index: int):
	black_background.modulate.a = 0
	label.text = endings_resource[index].text
	var tween = create_tween()
	tween.tween_property(black_background,"modulate:a",1,1.0)
	root_ui.show()
	
#-----------------------------
## -
#----------------------------
func _on_restart_pressed():
	get_tree().reload_current_scene()

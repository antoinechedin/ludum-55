class_name Fader extends Panel

func set_shader_alpha(value: float):
	self.material.set_shader_parameter("alpha", value)

func fade_in(duration: float):
	var tween = get_tree().create_tween()
	tween.tween_method(self.set_shader_alpha, 0.0, 1.0, duration)
	
func fade_out(duration: float):
	var tween = get_tree().create_tween()
	tween.tween_method(self.set_shader_alpha, 1.0, 0.0, duration)
	

extends Node3D

@onready var gui_panel = $GUIPanel3D
@onready var credits_panel = $Credits3D
@onready var animationPlayer = $CameraTransitions 

@onready var fader

func _ready():
	gui_panel.main_button.pressed.connect(self._go_to_credits)
	credits_panel.backButton.pressed.connect(self._go_to_main_menu)

func _go_to_credits():
	animationPlayer.play("main_to_credits")

func _go_to_main_menu():
	animationPlayer.play("credits_to_main")

func _button_pressed():
	print("Hello world!")

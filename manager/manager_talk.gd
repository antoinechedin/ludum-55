extends Node
class_name ManagerTalk

@export var ui_root: Control
@export var subviewport: SubViewport
@export var rich_text_label: RichTextLabel
@export var audio_player: AudioStreamPlayer3D
@export var manager_node: Node3D
@export var bubble_node: Sprite3D
@export var click_area_3d: Area3D
@export var blinking_pointer: TextureRect
@export var key_label: Label
@export var start_talking : GameEvent
@export var stop_talking : GameEvent
@export var custom_pos_event : GameEventAny

var custom_pos_node: Node3D = null
@export_group("Talk")
@export var talk_audio_samples: Array[AudioStream]
@export var talk_audio_player: AudioStreamPlayer3D
@export var set_anim_event: GameEventString
@export var change_speaker_event: GameEventString

var timer_max: float = 0.0
var timer: float = 0.0
var dialogue_line: DialogueLine

var line_started: bool = false
var waiting_for_input: bool = false

var pitch_variation: float = 0.0

var current_dialog: DialogueLine
#-----------------------------
## -
#-----------------------------
func _ready() -> void:
	blinking_pointer.hide()
	bubble_node.hide()
	if (custom_pos_event):
		custom_pos_event.subscribe(_set_custom_pos_node)
	
	ui_root.reparent.call_deferred(subviewport)
	DialogueManager.line_emited.connect(_on_dm_line_emited)
		
	click_area_3d.input_event.connect(_click_input_event)
	
	var input_event: InputEventKey = InputMap.action_get_events("dialogue_pass")[0]
	var keycode = DisplayServer.keyboard_get_keycode_from_physical(input_event.physical_keycode)
	key_label.text = OS.get_keycode_string(keycode)
	
	set_anim_event.subscribe(_anim_change)
	change_speaker_event.subscribe(_speaker_change)

func _set_custom_pos_node(node : Node3D):
	custom_pos_node = node


#-----------------------------
## -
#-----------------------------
func _on_dm_line_emited(line: DialogueLine):
	dialogue_line = line
	bubble_node.show()
	current_dialog = line
	line_started = true
	rich_text_label.text = line.text
	rich_text_label.visible_ratio = 0
	audio_player.stream = line.voice_acting
	audio_player.play()
	if(start_talking):
		start_talking.invoke()
	if line.voice_acting:
		timer_max = line.voice_acting.get_length()
	else:
		timer_max = line.text.length() * 0.02
	timer = timer_max

#-----------------------------
## -
#-----------------------------
func _process(delta: float) -> void:
	if (custom_pos_node):	
		bubble_node.global_position = custom_pos_node.global_position 
	else:
		bubble_node.position.z = manager_node.position.z
	
	if Input.is_action_just_pressed("dialogue_pass"):
		_dialogue_pass()
	if timer > 0:
		timer -= delta
		rich_text_label.visible_ratio = (timer_max-timer)/timer_max
		if not audio_player.playing and not talk_audio_player.playing:
			talk_audio_player.stream = talk_audio_samples.pick_random()
			talk_audio_player.pitch_scale = randf_range(1.4,1.8) + pitch_variation
			talk_audio_player.play()
	elif line_started :
		if(stop_talking):
			stop_talking.invoke()
		line_started = false
		if "key_list" in dialogue_line.next:
			current_dialog.lineEnded()
		else:
			waiting_for_input = true
			blinking_pointer.show()

#-----------------------------
## -
#-----------------------------
func _click_input_event(camera: Node, event: InputEvent, position: Vector3, normal: Vector3, shape_idx: int):
	if event is InputEventMouseButton and event.is_pressed() and event.button_index == MOUSE_BUTTON_LEFT:
		_dialogue_pass()

#-----------------------------
## -
#-----------------------------
func _dialogue_pass():
	if(stop_talking):
		stop_talking.invoke()
	if waiting_for_input:
		current_dialog.lineEnded()
		blinking_pointer.hide()
	elif line_started:
		timer = 0
		rich_text_label.visible_ratio = -1

#-----------------------------
## -
#-----------------------------
func _anim_change(anim_name: String):
	match anim_name:
		"Panic": pitch_variation = 0.25
		"Scared": pitch_variation = 0.25
		_: pitch_variation = 0.0

#-----------------------------
## -
#-----------------------------
func _speaker_change(_speaker: String):
	pitch_variation = -1.0

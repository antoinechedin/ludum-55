extends Node

@export var audio_stream_player: AudioStreamPlayer
@export var music_event: Array[GameEvent]
@export var music_streams: Array[AudioStream]
@export var music_volume: Array[int]
@export var stop_music_event: GameEvent

#-----------------------------
## -
#-----------------------------
func _ready() -> void:
	for i in music_event.size(): 
		music_event[i].subscribe(_play_music.bind(music_streams[i],music_volume[i]))
	stop_music_event.subscribe(_stop_music)

#-----------------------------
## -
#-----------------------------
func _play_music(music: AudioStream, volume: int):
	if audio_stream_player.playing:
		audio_stream_player.stop()
	audio_stream_player.stream = music
	audio_stream_player.volume_db = volume
	audio_stream_player.play()

#-----------------------------
## -
#-----------------------------
func _stop_music():
	audio_stream_player.stop()

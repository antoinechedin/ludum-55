class_name Const

const ArmR = "Arm_R1"
const ArmL = "Arm_L1"
const ArmR_Bis = "Arm_R2"
const ArmL_Bis = "Arm_L2"

const LegR = "Leg_R"
const LegL = "Leg_L"

const FootR = "Foot_R"
const FootL = "Foot_L"

const HandR = "Hand_R1"
const HandL = "Hand_L1"
const HandR_Bis = "Hand_R2"
const HandL_Bis = "Hand_L2"

const WingR = "Wing_R"
const WingL = "Wing_L"

const Hips = "Hips"
const Chest = "Chest"
const ChestFront = "ChestFront"
const Neck = "Neck"
const Head = "Head"
const Hat = "Hat"
const Tail = "Tail"

const id_to_packed_scene = {
	"Face_Normal":"res://art/sumo/parts/glb/SM_Sumo_Face_Normal.glb",
"Head_Normal":"res://art/sumo/parts/glb/SM_Sumo_Head_Normal.glb",
"Arm_R_Normal":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_Normal.glb",
"Arm_L_Normal":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_Normal.glb",
"Torso_Normal":"res://art/sumo/parts/glb/SM_Sumo_Torso_Normal.glb",
"Leg_R_Normal":"res://art/sumo/parts/glb/SM_Sumo_Leg_R_Normal.glb",
"Leg_L_Normal":"res://art/sumo/parts/glb/SM_Sumo_Leg_L_Normal.glb",
"Pants_Normal":"res://art/sumo/parts/glb/SM_Sumo_Pants_Normal.glb",
"Hat_Sumo":"res://art/sumo/parts/glb/SM_Sumo_Hat_Sumo.glb",
"Torso_Skinny":"res://art/sumo/parts/glb/SM_Sumo_Torso_Skinny.glb",
"Arm_L_Skinny":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_Skinny.glb",
"Arm_R_Skinny":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_Skinny.glb",
"Leg_R_Skinny":"res://art/sumo/parts/glb/SM_Sumo_Leg_R_Skinny.glb",
"Leg_L_Skinny":"res://art/sumo/parts/glb/SM_Sumo_Leg_L_Skinny.glb",
"Pants_Skinny":"res://art/sumo/parts/glb/SM_Sumo_Pants_Skinny.glb",
"Pants_Underwear":"res://art/sumo/parts/glb/SM_Sumo_Pants_Underwear.glb",
"Hat_TopHat":"res://art/sumo/parts/glb/SM_Sumo_Hat_TopHat.glb",
"Hat_Eye":"res://art/sumo/parts/glb/SM_Sumo_Hat_Eye.glb",
"Head_Eye":"res://art/sumo/parts/glb/SM_Sumo_Head_Eye.glb",
"Eye":"res://art/sumo/parts/glb/SM_Sumo_Eye.glb",
"Arm_L_Tentacle":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_Tentacle.glb",
"Arm_R_Tentacle":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_Tentacle.glb",
"Leg_R_Tentacle":"res://art/sumo/parts/glb/SM_Sumo_Leg_R_Tentacle.glb",
"Leg_L_Tentacle":"res://art/sumo/parts/glb/SM_Sumo_Leg_L_Tentacle.glb",
"Tentacle":"res://art/sumo/parts/glb/SM_Sumo_Tentacle.glb",
"Wing_L_Angel":"res://art/sumo/parts/glb/SM_Sumo_Wing_L_Angel.glb",
"Wing_R_Angel":"res://art/sumo/parts/glb/SM_Sumo_Wing_R_Angel.glb",
"Arm_R_Short":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_Short.glb",
"Arm_L_Short":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_Short.glb",
"Arm_R_Long":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_Long.glb",
"Arm_L_Long":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_Long.glb",
"Arm_R_Claws":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_Claws.glb",
"Arm_L_Claws":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_Claws.glb",
"Hat_FunnyHaircut":"res://art/sumo/parts/glb/SM_Sumo_Hat_FunnyHaircut.glb",
"Head_2D":"res://art/sumo/parts/glb/SM_Sumo_Head_2D.glb",
"Arm_R_2D":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_2D.glb",
"Arm_L_2D":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_2D.glb",
"Torso_2D":"res://art/sumo/parts/glb/SM_Sumo_Torso_2D.glb",
"Leg_R_2D":"res://art/sumo/parts/glb/SM_Sumo_Leg_R_2D.glb",
"Leg_L_2D":"res://art/sumo/parts/glb/SM_Sumo_Leg_L_2D.glb",
"Pants_2D":"res://art/sumo/parts/glb/SM_Sumo_Pants_2D.glb",
"Hat_2D":"res://art/sumo/parts/glb/SM_Sumo_Hat_2D.glb",
"Wing_R_2D":"res://art/sumo/parts/glb/SM_Sumo_Wing_R_2D.glb",
"Wing_L_2D":"res://art/sumo/parts/glb/SM_Sumo_Wing_L_2D.glb",
"Tail_2D":"res://art/sumo/parts/glb/SM_Sumo_Tail_2D.glb",
"Wing_R_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Wing_R_DemonKing.glb",
"Wing_L_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Wing_L_DemonKing.glb",
"Hat_Demonking":"res://art/sumo/parts/glb/SM_Sumo_Hat_Demonking.glb",
"Tail_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Tail_DemonKing.glb",
"Pants_Demonking":"res://art/sumo/parts/glb/SM_Sumo_Pants_Demonking.glb",
"Arm_R_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_DemonKing.glb",
"Arm_L_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_DemonKing.glb",
"Leg_R_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Leg_R_DemonKing.glb",
"Leg_L_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Leg_L_DemonKing.glb",
"Torso_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Torso_DemonKing.glb",
"Head_DemonKing":"res://art/sumo/parts/glb/SM_Sumo_Head_DemonKing.glb",
"Hat_Golden":"res://art/sumo/parts/glb/SM_Sumo_Hat_Golden.glb",
"Pants_Golden":"res://art/sumo/parts/glb/SM_Sumo_Pants_Golden.glb",
"Arm_R_Golden":"res://art/sumo/parts/glb/SM_Sumo_Arm_R_Golden.glb",
"Arm_L_Golden":"res://art/sumo/parts/glb/SM_Sumo_Arm_L_Golden.glb",
"Leg_R_Golden":"res://art/sumo/parts/glb/SM_Sumo_Leg_R_Golden.glb",
"Leg_L_Golden":"res://art/sumo/parts/glb/SM_Sumo_Leg_L_Golden.glb",
"Torso_Golden":"res://art/sumo/parts/glb/SM_Sumo_Torso_Golden.glb",
"Head_Golden":"res://art/sumo/parts/glb/SM_Sumo_Head_Golden.glb"
}


extends Node

@export_group("Symbols")
@export var symbols_display_event: GameEventAny
@export var symbols_undisplay_event: GameEvent
@export var on_symbol_pressed_event: GameEventAny
@export var symbol_display_pressed_event: GameEventAny
@export var symbols_start_timer_event: GameEventInt
@export var symbols_timer_timeout_event: GameEvent
@export var symbols_despawned_event: GameEvent
@export_group("Sumo")
@export var part_change_event: EventPartChange


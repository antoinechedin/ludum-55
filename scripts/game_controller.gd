extends Node3D

@export var skipMenu: bool = false

@onready var fader: Fader = $Fader
@onready var cameraAnimatonPlayer: AnimationPlayer = $ANIMATION_STUFF/CameraTransitions
@onready var mainMenu: Control = $MainMenu
@onready var playButton: BaseButton = $MainMenu/Play
@onready var creditsButton: BaseButton = $MainMenu/Credits
@onready var backToMainButton3D: Button3D = $MagicCubeForMenu/Back
@export var drum_audio_player: AudioStreamPlayer
@export var title_screen_music_event: GameEvent
@export var stop_music_event: GameEvent

var _start_story_node : StoryNode

func _ready():
	creditsButton.pressed.connect(self.go_to_credits)
	backToMainButton3D.pressed.connect(self.leave_credits)
	playButton.pressed.connect(self.go_to_play)
	
	title_screen_music_event.invoke()
	_start_story_node = load("res://resources/story/generated/Start.tres")
	# Skip main menu
	if skipMenu:
		enable_main_menu_buttons(false)
		enable_credits_buttons(true)
		mainMenu.modulate.a = 0.0
		return
	
	fader.set_shader_alpha(0.0)
	fader.fade_out(2.0)	
	cameraAnimatonPlayer.play("main")
	enable_main_menu_buttons(true)
	enable_credits_buttons(false)
	pass
	
func go_to_credits():
	drum_audio_player.play()
	var tween = get_tree().create_tween()
	tween.tween_callback(cameraAnimatonPlayer.play.bind("main_to_credits"))
	tween.tween_callback(self.enable_main_menu_buttons.bind(false))
	tween.tween_callback(self.enable_credits_buttons.bind(true))
	tween.tween_property(mainMenu, "modulate:a", 0.0, 0.3).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_CUBIC)
	pass
	
func leave_credits():
	drum_audio_player.play()
	var tween = get_tree().create_tween()
	tween.tween_callback(cameraAnimatonPlayer.play.bind("credits_to_main"))
	tween.tween_callback(self.enable_credits_buttons.bind(false))
	tween.tween_interval(cameraAnimatonPlayer.current_animation_length)
	tween.tween_callback(self.enable_main_menu_buttons.bind(true))
	tween.tween_property(mainMenu, "modulate:a", 1.0, 0.3).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_CUBIC)
	pass
	
func go_to_play():
	drum_audio_player.play()
	stop_music_event.invoke()
	var tween = get_tree().create_tween()
	tween.tween_callback(cameraAnimatonPlayer.play.bind("main_to_play"))
	tween.tween_callback(self.enable_main_menu_buttons.bind(false))
	tween.tween_callback(self.enable_credits_buttons.bind(false))
	tween.tween_property(mainMenu, "modulate:a", 0.0, 0.3).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_CUBIC)
	cameraAnimatonPlayer.animation_finished.connect(play)
	pass
	
func enable_main_menu_buttons(enable: bool):
	playButton.mouse_filter = Control.MOUSE_FILTER_STOP if enable else Control.MOUSE_FILTER_IGNORE
	creditsButton.mouse_filter = Control.MOUSE_FILTER_STOP if enable else Control.MOUSE_FILTER_IGNORE
	pass

func enable_credits_buttons(enable: bool):
	backToMainButton3D.enable = enable
	pass
	
func play(_crap):
	_start_story_node.run()

func _process(delta: float) -> void:
	if OS.is_debug_build():
		if Input.is_key_pressed(KEY_C):
			var unlock_event: GameEventInt = load("res://resources/story/generated_events/unlockFace.tres") 
			for i in range(6):
				unlock_event.invoke(i)

extends Resource
class_name EventBoneTransform

signal event(bone_name: String, value: Vector3)

func subscribe(function : Callable):
	event.connect(function)

func unsubscribe(function : Callable):
	event.disconnect(function)

func invoke(bone_name: String, scale: Vector3):
	event.emit(bone_name, scale)

extends Resource
class_name EventPartChange

signal event(bone_name: String, part: PackedScene, replace: bool)

func subscribe(function : Callable):
	event.connect(function)

func unsubscribe(function : Callable):
	event.disconnect(function)

func invoke(bone_name: String, part: PackedScene, replace: bool):
	event.emit(bone_name, part, replace)

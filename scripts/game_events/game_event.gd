extends Resource
class_name GameEvent

signal event

func subscribe(function : Callable):
	event.connect(function)

func unsubscribe(function : Callable):
	event.disconnect(function)

func invoke():
	event.emit()

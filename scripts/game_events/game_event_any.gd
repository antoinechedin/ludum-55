extends Resource
class_name GameEventAny

signal event(arg)

func subscribe(function : Callable):
	event.connect(function)

func unsubscribe(function : Callable):
	event.disconnect(function)

func invoke(arg):
	event.emit(arg)

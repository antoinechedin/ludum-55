extends Resource
class_name GameEventFloat

signal event(value: float)

func subscribe(function : Callable):
	event.connect(function)

func unsubscribe(function : Callable):
	event.disconnect(function)

func invoke(value: float):
	event.emit(value)

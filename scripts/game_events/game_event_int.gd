extends Resource
class_name GameEventInt

signal event(value: int)

func subscribe(function : Callable):
	event.connect(function)

func unsubscribe(function : Callable):
	event.disconnect(function)

func invoke(value: int):
	event.emit(value)

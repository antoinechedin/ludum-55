extends Resource
class_name GameEventString

signal event(value: String)

func subscribe(function : Callable):
	event.connect(function)

func unsubscribe(function : Callable):
	event.disconnect(function)

func invoke(value: String):
	event.emit(value)

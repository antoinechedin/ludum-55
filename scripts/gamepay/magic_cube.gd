extends Node
class_name MagicCube

const FACE_FRONT = 0
const FACE_RIGHT = 1
const FACE_UP = 2

const FACE_BACK = 3
const FACE_LEFT = 4
const FACE_DOWN = 5

@onready var cube_buttons = [
	[$VisualHolder/FaceFront/F1, $VisualHolder/FaceFront/F2, $VisualHolder/FaceFront/F3],
	[$VisualHolder/FaceRight/R1, $VisualHolder/FaceRight/R2, $VisualHolder/FaceRight/R3],
	[$VisualHolder/FaceUp/U1, $VisualHolder/FaceUp/U2, $VisualHolder/FaceUp/U3],
	[$VisualHolder/FaceBack/B1],
	[$VisualHolder/FaceLeft/L1, $VisualHolder/FaceLeft/L2, $VisualHolder/FaceLeft/L3],
	[$VisualHolder/FaceDown/D1, $VisualHolder/FaceDown/D2, $VisualHolder/FaceDown/D3],
]

@export var unlocked_face = [true, false, false, false, false, false]
@export var visual : Node3D
@export var disableButtons : GameEvent

@export var particlesApparition: GPUParticles3D
@export var cubeLight: OmniLight3D
@export var flagAnimation: AnimationPlayer

var up_axis : Vector3 = Vector3.UP

# Events
@export var unlockFace : GameEventInt

# Current cube state
# Front - right - back - left
var line = [FACE_FRONT, FACE_RIGHT, FACE_BACK, FACE_LEFT]
var up = FACE_UP
var down = FACE_DOWN

#For visual lerping
var visual_rot_start: Quaternion
var visual_rot_target: Quaternion

# Unlocking face animation
var animation_ready: bool = false

func unlock_face(face: int):
	if (!animation_ready): # Play particle and light animation
		animation_ready = true
		var tween = create_tween()
		if particlesApparition != null:
			tween.tween_property(particlesApparition, "emitting", true, 0.0)
		if cubeLight != null:
			tween.tween_property(cubeLight, "light_energy", 2, 0)
			tween.tween_property(cubeLight, "light_energy", 0, 0.8).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_CUBIC)
			tween.tween_property(self, "animation_ready", false, 0.0)
	
	if (face == 0): # First unlock face spawns cube
		if (flagAnimation != null):
			flagAnimation.play("fall")
		var tween = create_tween()
		tween.tween_property(visual, "scale", Vector3.ONE, 0.5).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_BACK)
	else: # We play an animation on the face unlocked
		for cube_button: CubeButton in cube_buttons[face]:
			cube_button.shaderMask = 6
			cube_button.targetMask = 0
			cube_button.shaderMaskSpeed = 0.02
	
	enableButtons(face, true)
	unlocked_face[face] = true


func lock_face(face: int):
	unlocked_face[face] = false

func is_unlocked(face: int) -> bool:
	return unlocked_face[face]

func get_opposite_face(face: int):
	return ((face + 3) % 6)

func rotate_cube(dir : Vector2):
	if (dir.x > 0 && is_unlocked(line.back())):
		line.push_front(line.pop_back())
		_rotate_visual(dir)
	elif (dir.x < 0 && is_unlocked(line[1])):
		line.push_back(line.pop_front())
		_rotate_visual(dir)
	elif (dir.y > 0 && is_unlocked(down)):
		up = line.front()
		line[0] = down
		line[2] = get_opposite_face(down)
		down = get_opposite_face(up)
		_rotate_visual(dir)
	elif (dir.y < 0 && is_unlocked(up)):
		down = line.front()
		line[0] = up
		line[2] = get_opposite_face(up)
		up = get_opposite_face(down)
		_rotate_visual(dir)

	print("------")
	print(up)
	print(line)
	print(down)
	pass

func _ready():
	up_axis = visual.basis.y
	unlockFace.subscribe(unlock_face)
	visual.scale = Vector3.ZERO
	enableAllButtons(false)
	disableButtons.subscribe(enableAllButtons.bind(false))

func _set_visual_rot(t):
	var q : Quaternion = visual_rot_start.slerp(visual_rot_target, t)
	visual.basis = Basis(q)
	pass

func _rotate_visual(dir : Vector2):
	if (dir.x != 0):
		visual_rot_target = Quaternion(up_axis, PI/2 * dir.x) * visual_rot_target
		visual_rot_start = Quaternion(visual.basis)
	elif (dir.y != 0):
		visual_rot_target = Quaternion(Vector3.LEFT, PI/2 * dir.y) * visual_rot_target
		visual_rot_start = Quaternion(visual.basis)

	var tween : Tween = create_tween()
	tween.tween_method(_set_visual_rot, 0.0, 1.0, 0.4).set_trans(Tween.TRANS_BACK).set_ease(Tween.EASE_OUT)

func _input(event):
	if event is InputEventKey and event.is_pressed() and event.is_echo() == false:
		if event.keycode == KEY_UP || event.physical_keycode == KEY_W:
			rotate_cube(Vector2(0, -1))
		if event.keycode == KEY_DOWN || event.physical_keycode == KEY_S:
			rotate_cube(Vector2(0, 1))
		if event.keycode == KEY_RIGHT || event.physical_keycode == KEY_D:
			rotate_cube(Vector2(-1, 0))
		if event.keycode == KEY_LEFT || event.physical_keycode == KEY_A:
			rotate_cube(Vector2(1, 0))

func enableAllButtons(value: float):
	for face in cube_buttons:
		for button: CubeButton in face:
			button.enableButton(value)
			
func enableButtons(face: int, value:bool):
	for button: CubeButton in cube_buttons[face]:
			button.enableButton(value)

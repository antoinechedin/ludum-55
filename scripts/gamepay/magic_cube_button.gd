class_name CubeButton extends Node

@export var clickableArea : Area3D
@export var onSymbolPressed : GameEventAny
@export var symbol : Symbol
@export var lightMaterial : Material
@export var cubeMaterial : Material
@export var shimmer_audio_player: AudioStreamPlayer3D
@export var pressed_audio_player: AudioStreamPlayer3D

var isHovering : bool
var startScale : Vector3
var stop_audio_tween: Tween

# Shader Controls
var shaderMask: float
var shaderMaskSpeed: float
var targetMask: float
var maskName: String

var _enabled = true

# Called when the node enters the scene tree for the first time.
func _ready():
	assert(lightMaterial)
	assert(cubeMaterial)
	clickableArea.mouse_entered.connect(mouse_entered)
	clickableArea.mouse_exited.connect(mouse_exited)
	maskName = "mask" + ("X" if symbol.name[1] == '1' else ("Y" if symbol.name[1] == '2' else "Z"))
	pass # Replace with function body.

func mouse_entered():
	if (!_enabled): return
	isHovering = true
	targetMask = .5
	shaderMaskSpeed = 0.1
	_audio_shimmer(true)
	pass 

func mouse_exited():
	if (!_enabled): return
	isHovering = false
	targetMask = 0.0
	_audio_shimmer(false)
	pass 

func _input(event):
	if (!_enabled): return
	if isHovering and event is InputEventMouseButton and event.is_released():
		assert(symbol)
		assert(onSymbolPressed)
		onSymbolPressed.invoke(symbol)
		shaderMask = 5.0
		targetMask = 0.0
		shaderMaskSpeed = 0.07
		## Audio
		pressed_audio_player.pitch_scale = randf_range(0.7,1.5)
		pressed_audio_player.play()
		_audio_shimmer(false)
		pass

func _process(delta: float):
	if (!_enabled): return
	shaderMask += (targetMask - shaderMask) * shaderMaskSpeed
	lightMaterial.set_shader_parameter(maskName, shaderMask)
	cubeMaterial.set_shader_parameter(maskName, shaderMask)
	pass

func _audio_shimmer(start: bool):
	return
	var audio_tween = shimmer_audio_player.create_tween()
	if start:
		if stop_audio_tween:
			stop_audio_tween.stop()
		audio_tween.tween_property(shimmer_audio_player,"volume_db",0,0.1)
		shimmer_audio_player.volume_db = -50
		shimmer_audio_player.seek(randf_range(0,shimmer_audio_player.stream.get_length()))
		#shimmer_audio_player.pitch_scale = randf_range(0.9,1.1)
		shimmer_audio_player.play()
	else:
		audio_tween.tween_property(shimmer_audio_player,"volume_db",-50,0.5)
		audio_tween.finished.connect(shimmer_audio_player.stop)
		stop_audio_tween = audio_tween

func enableButton(value: bool):
	if (value == false):
		mouse_exited()
	_enabled = value
	

class_name Pentagram extends Node

@export var material: Material
@export var changePartEvent: EventPartChange

# Shader Controls
var shaderMask: float
var shaderMaskSpeed: float
var targetMask: float
var maskName: String = "maskX"

func _ready():
	assert(material)
	assert(changePartEvent)
	changePartEvent.subscribe(glow)
	shaderMask = 0.0
	targetMask = 0.01
	pass

func glow(bone_name: String, part: PackedScene, replace: bool):
		shaderMask = 1.0
		targetMask = 0.01
		shaderMaskSpeed = 0.01
		pass

func _process(delta: float):
	shaderMask += (targetMask - shaderMask) * shaderMaskSpeed
	material.set_shader_parameter(maskName, shaderMask)
	pass

extends Skeleton3D

@export var scaleBoneEvent : EventBoneTransform
@export var moveBoneEvent : EventBoneTransform
@export var changePartEvent : EventPartChange
@export var resetAll : GameEvent
@export var assignMaterial : Material
@export var eyesMaterial : Material

# Called when the node enters the scene tree for the first time.
func _enter_tree():
	scaleBoneEvent.subscribe(on_scale_bone)
	moveBoneEvent.subscribe(on_move_bone)
	changePartEvent.subscribe(on_change_part)
	resetAll.subscribe(on_reset_all)
	pass # Replace with function body.

func get_all_children(in_node:Node, arr:=[]):
	arr.push_back(in_node)
	for child in in_node.get_children():
		arr = get_all_children(child,arr)
	return arr


func set_mat_on_all_children(part: Node):
	for child in get_all_children(part):
		if child is MeshInstance3D:
			if (child.name.ends_with("Eye")):
				child.set_material_override(eyesMaterial)
			else:
				child.set_material_override(assignMaterial)
	pass

func delayed_destroy(node :Node):
	node.queue_free()
	pass


func delete_with_anim(node :Node):
	var scaling_anim = create_tween()
	scaling_anim.tween_property(node, "scale", Vector3.ZERO, 0.3).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_CUBIC)
	scaling_anim.tween_callback(delayed_destroy.bind(node))
	pass

func on_reset_all():
	for bone in get_children():
		for todelete in bone.get_children():
			delete_with_anim(todelete)

func on_change_part(bone_name: String, part: PackedScene, replace: bool):
	for bone in get_children():
		if(bone.name == bone_name):
			if (replace):
				for bone_child in bone.get_children():
					bone_child.queue_free()
				pass
			pass
			var new_part : Node3D = part.instantiate()
			bone.add_child(new_part)
			new_part.global_rotation = get_parent().global_rotation
			new_part.position = Vector3.ZERO
			new_part.scale = Vector3.ZERO
			var scaling_anim = create_tween()
			scaling_anim.tween_property(new_part, "scale", Vector3.ONE, 0.3).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_BACK)
			set_mat_on_all_children(new_part)
			break
	pass

func on_move_bone(bone_name: String, offset: Vector3):
	for bone in get_children():
		if(bone.name == bone_name):
			bone.position = offset
			break
	pass

func on_scale_bone(bone_name: String, new_scale: Vector3):
	for bone in get_children():
		if(bone.name == bone_name):
			bone.scale = new_scale
			break
	pass
	

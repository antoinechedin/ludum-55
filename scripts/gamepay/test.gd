extends Node

@export var symbolPressed : GameEventAny
@export var unlockFace : GameEventInt
@export var replacePart : EventPartChange
@export var scaleBone : EventBoneTransform
@export var moveBone : EventBoneTransform
@export var scene : PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	
	
	unlockFace.invoke(MagicCube.FACE_UP)
	replacePart.invoke(Const.ArmL, scene, true)
	scaleBone.invoke(Const.ArmL, Vector3.ONE * 2)
	moveBone.invoke(Const.ArmL, Vector3.ONE * 2)

	symbolPressed.subscribe(log_name)
	
	pass # Replace with function body.

func log_name(symbol : Symbol):
	print(symbol.name)

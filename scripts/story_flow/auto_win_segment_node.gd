extends StoryNode

class_name AutoWinSegmentNode

@export var outputs : Array[StoryNode] # outputs[0] -> ok, output[1] -> neverUsed
@export var outputs_path : Array[String] # outputs[0] -> ok, output[1] -> neverUsed

func run():
	EventLibrary.on_symbol_pressed_event.subscribe(_on_symbol_pressed_event_invoken)

func _on_symbol_pressed_event_invoken(symbol: Symbol):
	outputs[0].run()

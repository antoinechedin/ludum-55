extends StoryNode

class_name EventNode

@export var event : Resource
@export var args = []

func run():
	if (event):
		if (args.size() == 1):
			event.invoke(args[0])
		elif (args.size() == 2):
			event.invoke(args[0], args[1])
		elif (args.size() == 3):
			event.invoke(args[0], args[1], args[2])
		else:
			event.invoke()
		
	super.run()

	pass

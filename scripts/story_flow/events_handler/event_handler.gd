extends Node

class_name EventHandler
@export var event : GameEvent

# Called when the node enters the scene tree for the first time.
func _ready():
	event.subscribe(_event_handler)
	pass


func _event_handler():
	pass

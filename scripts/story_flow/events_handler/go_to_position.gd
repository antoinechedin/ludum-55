extends EventHandler

@export var target : Node3D
@export var animation_player : AnimationPlayer
@export var visual : Node3D
@export var speed : float

var previous_anim : String

func _event_handler():
	var anim_time : float = target.position.distance_to(visual.position) / speed
	var move_anim : Tween = create_tween()
	visual.visible = true
	move_anim.tween_property(visual, "position", target.position, anim_time)
	move_anim.tween_callback(_reached_dest)
	previous_anim = animation_player.current_animation
	animation_player.play("Walk")
	visual.look_at(visual.global_position - (target.global_position - visual.global_position), target.global_basis.y)
	pass

func _reached_dest():
	animation_player.play(previous_anim)


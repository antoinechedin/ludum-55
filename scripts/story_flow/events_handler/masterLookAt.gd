extends Node
#Should use EventStringHandler
@export var masterLookAt : GameEventString
@export var master_root : Node3D
@export var y_rotate_to_player = 0;
@export var y_rotate_to_sumo = PI/2;

var current_rot = 0
var visual_rot_start : Quaternion
var visual_rot_target : Quaternion

func _ready():
	masterLookAt.subscribe(look_at)
	visual_rot_target = Quaternion(master_root.basis)

func _set_visual_rot(t):
	var q : Quaternion = visual_rot_start.slerp(visual_rot_target, t)
	master_root.basis = Basis(q)
	pass


func look_at(target : String):
	var angle = 0
	if(target == "player"):
		angle = y_rotate_to_player - current_rot
		current_rot = y_rotate_to_player
	elif(target == "sumo"):
		angle = y_rotate_to_sumo - current_rot
		current_rot = y_rotate_to_sumo

	visual_rot_target = Quaternion(master_root.basis.y, angle) * visual_rot_target
	visual_rot_start = Quaternion(master_root.basis)
	
	var tween : Tween = create_tween()
	tween.tween_method(_set_visual_rot, 0.0, 1.0, 1.0).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)

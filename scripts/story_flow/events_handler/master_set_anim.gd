extends EventStringHandler

@export var anim_player : AnimationPlayer

func _event_handler(param:String):
	anim_player.play(param)

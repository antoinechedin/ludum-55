extends Node

@export var startTalking: GameEvent
@export var stopTalking: GameEvent
@export var skeleton: Skeleton3D
@export var max_scale_x: float = 0.1
@export var max_scale_z: float = 0.3
@export var scale_time: float = 0.5

var bone_idx: int 
var animtween:Tween

func _set_bone_scaley(scale: float):
	var vscale = Vector3.ONE
	vscale.x += scale * max_scale_x
	vscale.z += (1 - scale) * max_scale_z
	skeleton.set_bone_pose_scale(bone_idx, vscale)
	pass

func _set_bone_scale(scale: float):
	var vscale = Vector3.ONE 
	vscale.x = scale
	vscale.z = scale
	skeleton.set_bone_pose_scale(bone_idx, vscale)
	pass
	

func _ready():
	for i in skeleton.get_bone_count():
		if(skeleton.get_bone_name(i) == "Mouth"):
			bone_idx = i
			break
		pass
	pass
	startTalking.subscribe(_start_talking)
	stopTalking.subscribe(_stop_talking)
	animtween = create_tween()
	animtween.set_loops()
	animtween.tween_method(self._set_bone_scaley, 0.0, 1.0, scale_time).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_QUAD)
	animtween.tween_method(_set_bone_scaley, 1.0, 0.0, scale_time).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_QUAD)
	animtween.pause()


func _start_talking():
	animtween.play()
	pass
	
func _stop_talking():
	animtween.pause()
	pass

extends EventHandler

@export var partChange : EventPartChange

var rng : RandomNumberGenerator

const possibleBones = [
	Const.ArmR,
	Const.ArmL,
	Const.ArmR_Bis,
	Const.ArmL_Bis,
	Const.LegR,
	Const.LegL,
	Const.WingR,
	Const.WingL,
	Const.Hips,
	Const.Chest,
	Const.ChestFront,
	Const.Head,
	Const.Hat,
	Const.Tail
]

const Head_List = [
"Head_Normal",
"Head_Eye",
"Head_DemonKing"]
const Hat_List = [
"Hat_TopHat",
"Hat_Sumo",
"Hat_Eye",
"Hat_FunnyHaircut",
"Hat_Demonking"]
const Arm_R_List = [
"Arm_R_Normal",
"Arm_R_Skinny",
"Arm_R_Tentacle",
"Arm_R_Short",
"Arm_R_Long",
"Arm_R_Claws",
"Arm_R_DemonKing"]
const Arm_L_List = ["Arm_L_Normal",
"Arm_L_Skinny",
"Arm_L_Tentacle",
"Arm_L_Short",
"Arm_L_Long",
"Arm_L_Claws",
"Arm_L_DemonKing"]
const Leg_R_List = [
"Leg_R_Normal",
"Leg_R_Skinny",
"Leg_R_Tentacle",
"Leg_R_DemonKing"]
const Leg_L_List = [
"Leg_L_Normal",
"Leg_L_Skinny",
"Leg_L_Tentacle",
"Leg_L_DemonKing"]
const Pants_List = [
"Pants_Normal",
"Pants_Skinny",
"Pants_Underwear",
"Pants_Demonking"]
const Torso_List = [
"Torso_Normal",
"Torso_Skinny",
"Torso_DemonKing"]
const Torso_Front = [
"Tentacle",
"Eye"]
const Wing_R_List = [
"Wing_R_Angel",
"Wing_R_DemonKing"]
const Wing_L_List = [
"Wing_L_Angel",
"Wing_L_DemonKing"]
const Tail_List = [
"Tail_DemonKing"]

const possibilityMapping = {
	Const.ArmR:Arm_R_List,
	Const.ArmL:Arm_L_List,
	Const.ArmR_Bis:Arm_R_List,
	Const.ArmL_Bis:Arm_L_List,
	Const.LegR:Leg_R_List,
	Const.LegL:Leg_L_List,
	Const.WingR:Wing_R_List,
	Const.WingL:Wing_L_List,
	Const.Hips:Pants_List,
	Const.Chest:Torso_List,
	Const.ChestFront:Torso_Front,
	Const.Head:Head_List,
	Const.Hat:Hat_List,
	Const.Tail:Tail_List
}

var dict = {}

func _set_part(bone_name, part_name):
	partChange.invoke(bone_name, ResourceLoader.load(Const.id_to_packed_scene[part_name]), true)
	pass

func _event_handler():
	rng = RandomNumberGenerator.new()
	rng.seed = hash("Godot")
	EventLibrary.on_symbol_pressed_event.subscribe(_on_symbol_pressed_event_invoken)

	pass

func _on_symbol_pressed_event_invoken(symbol: Symbol):
	rng.seed = hash(symbol.name)
	if symbol in dict:
		rng.state = dict[symbol]
	var bone_name = possibleBones[rng.randi_range(0,possibleBones.size()-1)]
	var part_name = possibilityMapping[bone_name][rng.randi_range(0,possibilityMapping[bone_name].size()-1)]
	dict[symbol] = rng.state
	_set_part(bone_name, part_name)
	pass

extends Node

@export var target : Node3D
@export var change_speaker : GameEventString
@export var set_pos_event : GameEventAny

func _ready():
	change_speaker.subscribe(_event_handler)
	pass

func _event_handler(_str:String):
	set_pos_event.invoke(target)
	pass

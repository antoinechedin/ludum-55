extends EventHandler

@export var partChange : EventPartChange


func _set_part(bone_name, part_name):
	partChange.invoke(bone_name, ResourceLoader.load(Const.id_to_packed_scene[part_name]), true)
	pass

func _set_skinny_part(bone_name, part_name):
	_set_part(bone_name, part_name + "_Skinny")
	pass

var delay = 0.2
func _event_handler():
	_set_skinny_part(Const.LegR, "Leg_R")
	await get_tree().create_timer(delay).timeout

	_set_skinny_part(Const.LegL, "Leg_L")
	await get_tree().create_timer(delay).timeout

	_set_skinny_part(Const.Hips, "Pants")
	await get_tree().create_timer(delay).timeout

	_set_skinny_part(Const.Chest, "Torso")
	await get_tree().create_timer(delay).timeout

	_set_skinny_part(Const.ArmL, "Arm_L")
	await get_tree().create_timer(delay).timeout

	_set_skinny_part(Const.ArmR, "Arm_R")
	await get_tree().create_timer(delay).timeout

	_set_part(Const.Head, "Head_Normal");

	pass

extends EventHandler

@export var animationPlayer : AnimationPlayer
@export var visual : Node3D
@export var height : float = 0.7


func _event_handler():
	var moveup : Tween = create_tween()
	moveup.tween_property(visual, "global_position:y", visual.global_position.y + height, 1.0).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN)
	animationPlayer.play("Levitate", 0.5, 0.5)
	pass

extends StoryNode

class_name ChangePartNode

@export var bone_name: String
@export var part: String
@export var replace: bool

func run():
	EventLibrary.part_change_event.invoke(bone_name, ResourceLoader.load(Const.id_to_packed_scene[part]), replace)
	super.run()

	pass

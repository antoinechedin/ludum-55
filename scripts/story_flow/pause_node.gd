extends StoryNode

class_name PauseNode

@export var time_to_wait : int # in seconds

func run():
	var tween = DialogueManager.get_tree().create_tween()

	tween.tween_interval(time_to_wait)
	tween.tween_callback(run_next)
	# await DialogueManager.get_tree().create_timer(time_to_wait).timeout

	pass

func run_next():
	next.run()


extends StoryNode

class_name SetBoolNode

@export var valueRef : BoolRef

func run():
	valueRef.value = true
	super.run()

extends StoryNode

class_name SpecialInputSegmentNode

@export var outputs : Array[StoryNode] # outputs[0] -> ok 1, # outputs[1] -> ok 2, output[2] -> fail
@export var outputs_path : Array[String] # outputs[0] -> ok 1, # outputs[1] -> ok 2, output[2] -> fail
@export var key_list: Array[Symbol]

var fail_count : int = 0
var key_index: int = 0

func run():
	var symbol_to_spawn: Array[Symbol] = [key_list[1]]
	EventLibrary.symbols_display_event.invoke(symbol_to_spawn)
	EventLibrary.on_symbol_pressed_event.subscribe(_on_symbol_pressed_event_invoken)

func _on_symbol_pressed_event_invoken(symbol: Symbol):
	EventLibrary.on_symbol_pressed_event.unsubscribe(_on_symbol_pressed_event_invoken)
	var next_node: StoryNode
	if symbol == key_list[0]:
		next_node = outputs[0]
		EventLibrary.symbol_display_pressed_event.invoke(Vector2(key_index,1))
	elif symbol == key_list[1]:
		next_node = outputs[1]
		EventLibrary.symbol_display_pressed_event.invoke(Vector2(key_index,1))
	else:
		next_node = outputs[2]
		EventLibrary.symbol_display_pressed_event.invoke(Vector2(key_index,0))
	EventLibrary.symbols_undisplay_event.invoke()
	EventLibrary.symbols_despawned_event.subscribe(_on_symbols_despawned.bind(next_node))
			
func _on_symbols_despawned(next_node: StoryNode):
	EventLibrary.symbols_despawned_event.unsubscribe(_on_symbols_despawned.bind(next_node))
	next_node.run()

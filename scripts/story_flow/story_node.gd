extends Resource

class_name StoryNode

@export var next : StoryNode
@export var next_path_ref : String

func run():
	if next: ## Ending
		next.run()
	pass

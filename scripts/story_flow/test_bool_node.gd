extends StoryNode

class_name TestBoolNode

@export var valueRef : BoolRef
@export var outputs : Array[StoryNode] # outputs[0] -> ok, output[1] -> fail
@export var outputs_path : Array[String]

func run():
	if valueRef.value:
		outputs[0].run()
	else :
		outputs[1].run()

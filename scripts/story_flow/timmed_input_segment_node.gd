extends StoryNode

class_name TimmedInputSegmentNode

@export var outputs : Array[StoryNode] # outputs[0] -> ok, output[1] -> fail
@export var outputs_path : Array[String]
@export var key_list: Array[Symbol] = []

@export var time_in_seconds: int

var fail_count : int = 0
var key_index: int = 0

func run():
	EventLibrary.symbols_display_event.invoke(key_list)
	EventLibrary.symbols_start_timer_event.invoke(time_in_seconds)
	EventLibrary.on_symbol_pressed_event.subscribe(_on_symbol_pressed_event_invoken)
	EventLibrary.symbols_timer_timeout_event.subscribe(_on_symbols_timer_timeout_event_invoken)

func _on_symbol_pressed_event_invoken(symbol: Symbol):
	if symbol != key_list[key_index]:
		fail_count += 1
		EventLibrary.symbol_display_pressed_event.invoke(Vector2(key_index,0))
	else:
		EventLibrary.symbol_display_pressed_event.invoke(Vector2(key_index,1))
	key_index += 1
	# End
	if key_index >= key_list.size():
		EventLibrary.symbols_start_timer_event.invoke(-1)
		_next_common()
		var next_node: StoryNode
		if fail_count > 0:
			next_node = outputs[1]
		else :
			next_node = outputs[0]
		EventLibrary.symbols_despawned_event.subscribe(_on_symbols_despawned.bind(next_node))

func _on_symbols_despawned(next_node: StoryNode):
	EventLibrary.symbols_despawned_event.unsubscribe(_on_symbols_despawned.bind(next_node))
	next_node.run()

func _on_symbols_timer_timeout_event_invoken():
	_next_common()
	EventLibrary.symbols_despawned_event.subscribe(_on_symbols_despawned.bind(outputs[1]))

func _next_common():
	EventLibrary.symbols_undisplay_event.invoke()
	EventLibrary.on_symbol_pressed_event.unsubscribe(_on_symbol_pressed_event_invoken)
	EventLibrary.symbols_timer_timeout_event.unsubscribe(_on_symbols_timer_timeout_event_invoken)

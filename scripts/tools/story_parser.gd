@tool
extends EditorScript

var script_path : String = 'res://senario/finalStory.txt'
var generated_folder : String = 'res://resources/story/generated/'
var events_folder : String = 'res://resources/story/generated_events/'
var values_folder : String = 'res://resources/story/generated_values/'
var symbols_folder : String = 'res://resources/symbols/'

# Balise mapping
var parse_create_func = {
	"#D": create_dialog_line,
	"#W": create_input_seg_node, #Acts as a conditionnal goto
	"#WB": create_test_value_node,
	"#WE": create_special_input_seg_node,
	"#WT": create_timed_input_seg_node,
	"#L": create_label,
	"#CI": create_event_node_int,
	"#CL": create_event_node_one_arg,
	"#CF": create_event_node_float,
	"#CS": create_event_node_string,
	"#C": create_event_node,
	"#E" : log_error_implementation,
	"#B" : create_set_value_node,
	"#P" : create_pause_node,
	"#scaleBone": log_error_implementation,
	"#partChange" : create_part_change
}


# Current internal state of parser
# Globals because needed across funcs
var last_node_name : String = ""
var line_number : int 
var args : PackedStringArray
var balise : String




###### UTILS #######
func link_and_save_node(node:StoryNode, name: String):
	save_node(node, name)
	link_to_last(name)
	last_node_name = name
	pass

func link_to_last(name : String):
	var last_node : StoryNode = load_node(last_node_name)
	if (last_node):
		if (last_node.next_path_ref.is_empty()):
			last_node.next_path_ref = get_node_path(name)
			save_node(last_node, last_node_name)
	else:
		push_warning("Parsing line " + str(line_number) + ": Previous node not found")

func get_args(line : String, args_min : int) -> bool:
	var regex : RegEx = RegEx.new()
	regex.compile(r"{(.*?)}")
	var results = regex.search_all(line)
	args = []
	for x in results:
		if(x.strings.size() > 1):
			args.append(x.strings[1])

	if (args.size() < args_min):
		push_error("Parsing line " + str(line_number) + " failed: Found " + str(args.size()) + " arguments. Expecting " + str (args_min))
		return false
	return true

func get_balise(line) -> bool:
	var regex = RegEx.new()
	regex.compile(r"(#\w*)")
	var result = regex.search(line)
	if (result == null):
		push_error("Parsing line " + str(line_number) + " failed: No pattern found")
		return false
	balise = result.get_string()
	return true

func create_or_load_event(name: String) -> Resource:
	var event_rsc : Resource = load_event(name)
	if (event_rsc != null):
		return event_rsc
	
	#Create because not exist
	if (balise == "#C"):
		event_rsc = GameEvent.new()
	elif (balise == "#CI"):
		event_rsc = GameEventInt.new()
	elif (balise == "#CF"):
		event_rsc = GameEventFloat.new()
	elif (balise == "#CS"):
		event_rsc = GameEventString.new()
	pass
	save_event(event_rsc, name)
	return event_rsc



func create_or_load_value(name: String) -> Resource:
	var value_rsc : Resource = load_value(name)
	if (value_rsc != null):
		return value_rsc
	
	#Create because not exist
	if (balise == "#B" || balise == "#WB"):
		value_rsc = BoolRef.new()
	pass

	save_value(value_rsc, name)
	return value_rsc


##### SAVE ######

func get_node_path(name: String) -> String:
	return generated_folder + name + ".tres"

func load_node(name : String) -> StoryNode:
	var load_path =  get_node_path(name)
	if FileAccess.file_exists(load_path):
		return ResourceLoader.load(load_path)
	else:
		return null

func load_event(name : String) -> Resource:
	var load_path =  events_folder + name + ".tres"
	if FileAccess.file_exists(load_path):
		return ResourceLoader.load(load_path)
	else:
		return null

func load_value(name : String) -> Resource:
	var load_path =  values_folder + name + ".tres"
	if FileAccess.file_exists(load_path):
		return ResourceLoader.load(load_path)
	else:
		return null

func load_symbol(name : String) -> Symbol:
	var load_path =  symbols_folder + name + ".tres"
	if FileAccess.file_exists(load_path):
		return ResourceLoader.load(load_path)
	else:
		return null


func save_node(object : StoryNode, name : String):
	var save_path = get_node_path(name)
	if (ResourceSaver.save(object, save_path) != OK):
		push_error("Resource " + save_path + " save failed ")
	pass
	
func save_event(object : Resource, name : String):
	var save_path =  events_folder + name + ".tres"
	if (ResourceSaver.save(object, save_path) != OK):
		push_error("Event " + save_path + " save failed ")
	pass

func save_value(object : Resource, name : String):
	var save_path =  values_folder + name + ".tres"
	if (ResourceSaver.save(object, save_path) != OK):
		push_error("ValRef " + save_path + " save failed ")
	pass


###### CREATE NODES ######
func log_error_implementation(line: String):
	push_warning("Not implemented " + line)
	pass


func create_dialog_line(line : String):
	if (get_args(line , 2) == false):
		return

	var dialogLine = DialogueLine.new()
	dialogLine.text = args[1];
	var nodeName = args[0]
	link_and_save_node(dialogLine, nodeName)


func create_label(line : String):
	if (get_args(line , 1) == false):
		return

	var storyNode = StoryNode.new()
	var nodeName = args[0]
	link_and_save_node(storyNode, nodeName)
	pass

func create_input_seg_node(line : String):
	if (get_args(line , 3) == false):
		return
	
	var nodeName = "W" + str(line_number)

	if (args[0].is_empty()):
		var nextNode = args[1]
		# Story node with custom next link
		var storyNode = StoryNode.new()
		storyNode.next_path_ref = get_node_path(nextNode)
		link_and_save_node(storyNode, nodeName)
	elif(args[0] == "*"):
		#Match any key, only need output 1
		var awSegNode = AutoWinSegmentNode.new()
		awSegNode.outputs_path.clear()
		awSegNode.outputs_path.push_back(args[1])
		link_and_save_node(awSegNode, nodeName)
	else:
		var inputSegNode = InputSegmentNode.new()
		inputSegNode.outputs_path.clear()
		inputSegNode.outputs_path.push_back(args[1])
		inputSegNode.outputs_path.push_back(args[2])

		for key in args[0].split(' '):
			inputSegNode.key_list.append(load_symbol(key.to_lower()))
		link_and_save_node(inputSegNode, nodeName)
	pass

#WE{L2 R2}{E1}{111}{112}
func create_special_input_seg_node(line : String):
	if (get_args(line , 4) == false):
		return
	
	var nodeName = "WE" + str(line_number)

	var inputSegNode = SpecialInputSegmentNode.new()
	inputSegNode.outputs_path.clear()
	inputSegNode.outputs_path.push_back(args[1])
	inputSegNode.outputs_path.push_back(args[2])
	inputSegNode.outputs_path.push_back(args[3])


	for key in args[0].split(' '):
		inputSegNode.key_list.append(load_symbol(key.to_lower()))
	assert(inputSegNode.key_list.size() == 2)
	link_and_save_node(inputSegNode, nodeName)
	pass

#WT{10}{R1 B R2 U3 F2}{2111a}{212}
func create_timed_input_seg_node(line : String):
	if (get_args(line , 4) == false):
		return
	
	var nodeName = "WT" + str(line_number) + "-" + str(args[0])

	var inputSegNode = TimmedInputSegmentNode.new()
	inputSegNode.outputs_path.clear()
	inputSegNode.outputs_path.push_back(args[2])
	inputSegNode.outputs_path.push_back(args[3])
	inputSegNode.time_in_seconds = int(args[0])

	for key in args[1].split(' '):
		inputSegNode.key_list.append(load_symbol(key.to_lower()))

	link_and_save_node(inputSegNode, nodeName)
	pass

func create_event_node_int(line : String):
	if (get_args(line , 2) == false):
		return
	create_event_node_one_arg(args[0], args[1].to_int())

	pass

func create_event_node_string(line : String):
	if (get_args(line , 2) == false):
		return
	create_event_node_one_arg(args[0], args[1])

	pass

func create_event_node_float(line : String):
	if (get_args(line , 2) == false):
		return
	create_event_node_one_arg(args[0], args[1].to_float())

	pass

func create_event_node_one_arg(name : String, arg):
	var eventNode = EventNode.new()
	var nodeName = "CX" + str(line_number) + "-" + name
	eventNode.event = create_or_load_event(name)
	eventNode.args.append(arg)
	link_and_save_node(eventNode, nodeName)
	pass


func create_event_node(line : String):
	if (get_args(line , 1) == false):
		return
	var eventNode = EventNode.new()
	var nodeName = "C" + str(line_number) + "-" + args[0]
	eventNode.event = create_or_load_event(args[0])
	link_and_save_node(eventNode, nodeName)
	pass

func create_set_value_node(line : String):
	if (get_args(line , 1) == false):
		return
	var valueNode = SetBoolNode.new()
	var nodeName = "B" + str(line_number) + "-" + args[0]
	valueNode.valueRef = create_or_load_value(args[0])
	link_and_save_node(valueNode, nodeName)
	pass

func create_test_value_node(line : String):
	if (get_args(line , 3) == false):
		return
	var valueNode = TestBoolNode.new()
	var nodeName = "WB" + str(line_number) + "-" + args[0]
	valueNode.valueRef = create_or_load_value(args[0])
	valueNode.outputs_path.clear()
	valueNode.outputs_path.append(args[2])#True
	valueNode.outputs_path.append(args[1])#False
	link_and_save_node(valueNode, nodeName)
	pass

func create_pause_node(line : String):
	if (get_args(line , 1) == false):
		return
	var pauseNode = PauseNode.new()
	var nodeName = "P" + str(line_number) + "-" + args[0]
	pauseNode.time_to_wait = int(args[0])
	link_and_save_node(pauseNode, nodeName)
	pass

func create_part_change(line : String):
	if (get_args(line , 3) == false):
		return
	var partChangeNode = ChangePartNode.new()
	var nodeName = "partChange" + str(line_number) + "-" + args[0] + "-" + args[1]
	partChangeNode.bone_name = args[0]
	partChangeNode.part = args[1]
	partChangeNode.replace = args[2] == "true"
	link_and_save_node(partChangeNode, nodeName)


###### LINK NODES ######
func link_next(node: StoryNode):
	if (node.next_path_ref != "" && FileAccess.file_exists(node.next_path_ref)):
		node.next = ResourceLoader.load(node.next_path_ref)
	if (node.next == null):
		push_warning("Linking node " + str(node.resource_path) + ": No next found " + node.next_path_ref)

func link_segment_output(node: StoryNode):
	node.outputs.clear()
	for i in node.outputs_path.size():
		var loaded_node = load_node(node.outputs_path[i])
		if (loaded_node != null):
			node.outputs.push_back(loaded_node)
		else:
			push_error("Linking node " + str(node.resource_path) + ": Output not found " + node.outputs_path[i])




###### PARSE ######
func parse_line(line: String):
	if (line.length() == 0 or line.begins_with("//")):
		return

	if (get_balise(line) == false):
		return

	if balise in parse_create_func:
		parse_create_func[balise].call(line)
	else:
		push_error("Parsing line " + str(line_number) + " failed: No such tag " + balise)
	pass


# Called when the script is executed (using File -> Run in Script Editor).
func _run():

	# Story node folder (delete all)
	if (DirAccess.dir_exists_absolute(generated_folder)):
		DirAccess.remove_absolute(generated_folder)
	DirAccess.make_dir_absolute(generated_folder)

	# Event folder
	if (DirAccess.dir_exists_absolute(events_folder) == false):
		DirAccess.make_dir_absolute(events_folder)

	# ValRef folder
	if (DirAccess.dir_exists_absolute(values_folder) == false):
		DirAccess.make_dir_absolute(values_folder)


	var file = FileAccess.open(script_path, FileAccess.READ)
	line_number = 1
	print(file)
	#Create all
	while not file.eof_reached() and line_number < 64000: #Guard against infinite loop (should not happen)
		var line = file.get_line()
		parse_line(line)
		line_number += 1

	file.close()

	#Link all
	for node_path in DirAccess.get_files_at(generated_folder):
		var node = ResourceLoader.load(generated_folder + node_path)
		if (node is StoryNode):
			link_next(node)
			if (node is InputSegmentNode or node is AutoWinSegmentNode or node is SpecialInputSegmentNode or node is TimmedInputSegmentNode or node is TestBoolNode):
				link_segment_output(node)
			ResourceSaver.save(node, generated_folder + node_path)
	pass

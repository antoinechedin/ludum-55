class_name Button3D extends Node3D

@onready var area3D: Area3D = $Area3D
signal pressed

var enable: bool
var isHovering : bool

func _ready():
	area3D.mouse_entered.connect(mouse_entered)
	area3D.mouse_exited.connect(mouse_exited)
	pass

func mouse_entered():
	isHovering = true
	pass 

func mouse_exited():
	isHovering = false
	pass

func _input(event):
	if enable and isHovering and event is InputEventMouseButton and event.is_pressed() and event.button_index == MOUSE_BUTTON_LEFT:
		pressed.emit()
		pass

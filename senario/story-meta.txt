//-------------------------------------------------------------------------------------------------------------------
// ASSETS
Runes : F1, F2, F3, R1, R2, R3, L1, L2, L3, U1, U2, U3, D1, D2, D3, B

Bones :
Arm_R1
Arm_L1
Arm_R2
Arm_L2
Leg_R
Leg_L
TODO // foot r
TODO // foot l
TODO // hand r
TODO // foot l
Wing_R
Wing_L
Hips
Chest
ChestFront
Head
Neck
Tail

expressionNormal = 0
expressionSick = 1

Cube Faces :
FACE_FRONT = 0
FACE_RIGHT = 1
FACE_UP = 2
FACE_BACK = 3
FACE_LEFT = 4
FACE_DOWN = 5

//-------------------------------------------------------------------------------------------------------------------
// COMMAND A PARSER

--#L{ID de label}
--#D{Nom dialogue/ressource audio}{Dialoguqe}
#W{Liste de runes à afficher}{ID #L réussite}{ID #L échec}
#C{Nom de l'event}
#CI{Nom de l'event}{Paramètre de l'event(int)}
#CF{Nom de l'event}{Paramètre de l'event(float)}
#CS{Nom de l'event}{Paramètre de l'event(string)}

#B{VarName} // Set a bool at true
#WB{VarName}{ID #L réussite}{ID #L échec} // Check if the bool has been set as true
#WE{RUNE1 RUNE2}{ID #L si RUNE1 Pressed}{ID #L si RUNE2 Pressed}{ID #L si échec}
#WT{Time to read the list}{Liste de runes à afficher}{ID #L réussite}{ID #L échec}

--#P{Nb second to pause before next instruction}

#partChange{boneName}{AssetName}{replace}
#scaleBone{boneName}{xScale}{yScale}{zScale}

@{Rune à afficher dans le texte}

//-------------------------------------------------------------------------------------------------------------------
// GENERIC EVEN LIST
masterLookAt("target")
changeSpeaker("newspeaker")
spawnSkinnySumo()
spawnGlitterParticles()
spawnPoopParticles()
spawnLevitatingSumosEveryWhere()
makeGoodSumo()
makePerfectSumo()
makeDemonKingSumo()
makeGateFromHellSumo()
sumoStartsLevitating()
masterLeaves()
masterComes()
giveMasterManual()
startGateToHellRandomKeyBinding()
startGlitchRandomKeyBinding()
killMaster()

startFireEndingScreen()
startPerfectSumoEndingScreen()
startDemonicEndingScreen()
startGatesFromHellEndingScreen()
startGlitchEndingScreen()
extends Node

@export var event: GameEventInt
@export var audio_streams: Array[AudioStream]
@export var audio_volume: int

#-----------------------------
## -
#-----------------------------
func _ready() -> void:
	event.subscribe(_play_effect)

#-----------------------------
## -
#-----------------------------
func _play_effect(face_index: int):
	if face_index == 0:
		for audio in audio_streams:
			var audio_stream_player: AudioStreamPlayer = AudioStreamPlayer.new()
			add_child(audio_stream_player)
			audio_stream_player.stream = audio
			audio_stream_player.volume_db = audio_volume
			audio_stream_player.play()

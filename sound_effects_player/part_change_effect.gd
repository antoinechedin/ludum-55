extends Node

@export var event: EventPartChange
@export var audio_stream: AudioStream
@export var audio_volume: int

#-----------------------------
## -
#-----------------------------
func _ready() -> void:
	event.subscribe(_play_effect.bind(audio_stream,audio_volume))

#-----------------------------
## -
#-----------------------------
func _play_effect(_i, _dont, _care, audio: AudioStream, volume: int):
	var audio_stream_player: AudioStreamPlayer = AudioStreamPlayer.new()
	add_child(audio_stream_player)
	audio_stream_player.stream = audio
	audio_stream_player.volume_db = volume
	audio_stream_player.play()

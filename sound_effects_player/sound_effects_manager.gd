extends Node

@export var root_node: Node
@export var events: Array[GameEvent]
@export var audio_streams: Array[AudioStream]
@export var audio_volumes: Array[int]

#-----------------------------
## -
#-----------------------------
func _ready() -> void:
	for i in events.size(): 
		events[i].subscribe(_play_effect.bind(audio_streams[i],audio_volumes[i]))

#-----------------------------
## -
#-----------------------------
func _play_effect(audio: AudioStream, volume: int):
	var audio_stream_player: AudioStreamPlayer = AudioStreamPlayer.new()
	root_node.add_child(audio_stream_player)
	audio_stream_player.stream = audio
	audio_stream_player.volume_db = volume
	audio_stream_player.play()

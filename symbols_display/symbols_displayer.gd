extends Node

@export var root_node: Node3D
@export var parent_node: Node3D
@export var symbol_material: ShaderMaterial
@export var symbol_list: Array[Symbol] # TEMP: need to do a connection to an event
@export var symbols_display_event: GameEventAny
@export var symbols_undisplay_event: GameEvent
@export var symbol_display_pressed: GameEventAny
@export var symbols_despawned_event: GameEvent
@export var audio_player: AudioStreamPlayer
@export var symbol_particles_scene: PackedScene
@export var flag_node: Node3D
@export var flag2_node: Node3D

#const WIDTH: float = 1
const WIDTH: float = 0.5
const OFFSET: float = 0
const APPEAR_DURATION: float = 0.3
const DISAPPEAR_DURATION: float = 0.2

#-----------------------------
## -
#-----------------------------
func _ready() -> void:
	symbols_display_event.subscribe(_spawn_symbols)
	symbols_undisplay_event.subscribe(_despawn_sumbols)
	symbol_display_pressed.subscribe(_on_symbol_pressed)

#func _process(delta: float) -> void:
	#if OS.is_debug_build():
		#if Input.is_action_just_pressed("ui_accept"):
			#_spawn_symbols(symbol_list)

#-----------------------------
## -
#-----------------------------
func _spawn_symbols(list: Array[Symbol]):
	for i in list.size():
		var symbol: Symbol = list[i]
		var symbol_node: MeshInstance3D = _create_symbol(symbol)
		#parent_node.add_child(symbol_node)
		flag_node.add_child(symbol_node)
		#var centerred_index: int = i - list.size()/2
		#symbol_node.position.x = WIDTH * centerred_index + OFFSET * centerred_index
		symbol_node.position.y = -WIDTH * i + -OFFSET * i
		var shader_material: ShaderMaterial = symbol_node.get_surface_override_material(0)
		shader_material["shader_parameter/albedo"] = Color.TRANSPARENT
		var tween: Tween = create_tween()
		tween.tween_property(symbol_node,"position:x",0.0,APPEAR_DURATION).from(0.1).set_delay(i*APPEAR_DURATION).set_trans(Tween.TRANS_BACK).set_ease(Tween.EASE_OUT)
		tween.parallel().tween_property(shader_material,"shader_parameter/albedo",Color.WHITE,APPEAR_DURATION).set_delay(i*APPEAR_DURATION).set_trans(Tween.TRANS_BACK).set_ease(Tween.EASE_OUT)
		tween.parallel().tween_property(audio_player,"playing",true,0.01).from(false).set_delay(i*APPEAR_DURATION)
		#tween.finished.connect(symbol_node.add_child.bind(symbol_particles_scene.instantiate()))
	
#-----------------------------
## -
#-----------------------------
func _create_symbol(symbol: Symbol) -> MeshInstance3D:
	var meshInstance3D: MeshInstance3D = MeshInstance3D.new()
	meshInstance3D.mesh = QuadMesh.new()
	var aspect_ratio = float(symbol.image.get_height()) / symbol.image.get_width()
	#var quad_size: Vector2 = Vector2(1,aspect_ratio).normalized()
	#meshInstance3D.mesh.size = quad_size
	aspect_ratio = 1.0/aspect_ratio
	var quad_size: Vector2 = Vector2(aspect_ratio,1).normalized()
	meshInstance3D.mesh.size = quad_size/2
	var shader_material: ShaderMaterial = symbol_material.duplicate()
	shader_material.set_shader_parameter("SCROLL",Vector2(randf()-0.5,randf()-0.5)/2)
	shader_material.set_shader_parameter("SCROLL2",Vector2(randf()-0.5,randf()-0.5)/2)
	#shader_material.set_shader_parameter("LEVITATE_SPEED",randf_range(2.5,3.5))
	shader_material.set_shader_parameter("LEVITATE_SPEED",0.0)
	shader_material.set_shader_parameter("texture_albedo",symbol.image)
	meshInstance3D.set_surface_override_material(0, shader_material)
	return meshInstance3D

#-----------------------------
## -
#-----------------------------
func _despawn_sumbols():
	var last_tween: Tween
	var child_count: int = parent_node.get_child_count()
	for i in range(child_count-1,-1,-1):
		var symbol_node: MeshInstance3D = parent_node.get_child(i)
		var shader_material: ShaderMaterial = symbol_node.get_surface_override_material(0)
		var tween: Tween = create_tween()
		tween.tween_property(symbol_node,"position:y",0.1,DISAPPEAR_DURATION).set_delay(i*DISAPPEAR_DURATION).set_trans(Tween.TRANS_BACK).set_ease(Tween.EASE_OUT)
		tween.parallel().tween_property(shader_material,"shader_parameter/albedo",Color.TRANSPARENT,DISAPPEAR_DURATION).set_delay(i*DISAPPEAR_DURATION).set_trans(Tween.TRANS_BACK).set_ease(Tween.EASE_OUT)
		#tween.parallel().tween_property(audio_player,"playing",true,0.01).from(false).set_delay(i*DISAPPEAR_DURATION)
		tween.finished.connect(symbol_node.queue_free)
		if i==child_count-1:
			last_tween = tween
	last_tween.finished.connect(_symbols_despawned)

#-----------------------------
## -
#-----------------------------
func _symbols_despawned():
	symbols_despawned_event.invoke()

#-----------------------------
## -
#-----------------------------
func _on_symbol_pressed(data: Vector2):
	var symbol_node: MeshInstance3D = parent_node.get_child(data.x)
	var shader_material: ShaderMaterial = symbol_node.get_surface_override_material(0)
	var tween: Tween = create_tween()
	var color: Color
	if data.y:
		color = Color.GREEN
	else:
		color = Color.RED
	tween.tween_property(shader_material,"shader_parameter/SYMBOL_COLOR",color,APPEAR_DURATION).set_trans(Tween.TRANS_BACK).set_ease(Tween.EASE_OUT)

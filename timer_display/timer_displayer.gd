extends Node

@export var timer:Timer
@export var display: MeshInstance3D
@export var symbols_start_timer_event: GameEventInt
@export var symbols_timer_timeout_event: GameEvent

#-----------------------------
## -
#-----------------------------
func _ready() -> void:
	display.hide()
	symbols_start_timer_event.subscribe(_on_symbols_start_timer_event)

#-----------------------------
## -
#-----------------------------
func _on_symbols_start_timer_event(value: int) -> void:
	if value == -1:
		_hide_timer()
	else:
		display.show()
		timer.wait_time = value
		timer.start()
		timer.timeout.connect(_on_timer_timeout)

#-----------------------------
## -
#-----------------------------
func _process(delta: float) -> void:
	var shader_matrial: ShaderMaterial = display.mesh.material
	shader_matrial["shader_parameter/progress"] = (timer.wait_time-timer.time_left) /  timer.wait_time

#-----------------------------
## -
#-----------------------------
func _on_timer_timeout() -> void:
	symbols_timer_timeout_event.invoke()
	_hide_timer()

#-----------------------------
## -
#-----------------------------
func _hide_timer():
	display.hide()
